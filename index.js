var through = require('through2');
var fs = require('fs');
var gutil = require('gulp-util');
var File = gutil.File;
var Buffer = require('buffer').Buffer;

module.exports = function() {

	// Regex to match debug blocks.
	var debugRegex = /((\s*\/\/\s*debug\s*)|(\/\*\s*debug\s*\*\/))([\s\S])*?((\/\/\s*\/\s*debug\s+)|(\/\*\s*\/debug\s*\*\/))/mig

	var clearDebugsFromFile = function(file, encoding, callback) {
		var result = file.contents.toString('utf8').replace(debugRegex, '');

		var file = new File ( { 'path' : file.path, 'contents' : new Buffer ( result ) } );
		this.emit ( 'data', file ); // jshint ignore:line
    	this.emit ( 'end' ); // jshint ignore:line

		callback();
	};

	return through.obj(clearDebugsFromFile);
}

